"""
Raw Data synchronization

Written by Constantin Ehrlich-Adám
pdz, ETH Zürich
2019
------------------------------------------------------------

Usage: run from the command line as such:
    # Raw data synchronization
    python3 raw_data_synchronization.py --device=name
"""

from Helper.raw_data_sync_helper import *

ROOT_DIR = './'

if __name__ == '__main__':
    import argparse

    print('Synchronize Raw Data with Video')

    parser = argparse.ArgumentParser(
        description='Raw Data synchronization with video playback')
    parser.add_argument('--device', required=False,
                        default='False',
                        metavar="device",
                        help='Device Name')
    args = parser.parse_args()

    print('Reading raw Data')
    pts = read_raw_data(args.device, ROOT_DIR)
    print('Reading video')
    video, nb_frames = read_video(args.device, ROOT_DIR)
    print('Matching gaze to frame')
    times_ranges = build_time_ranges(nb_frames)
    gaze = match_point_to_frame(pts,times_ranges)
    print('Writing to file')
    write_to_file(args.device, gaze, ROOT_DIR)



