"""
Optical Flow tracking

Ehrlich-Adám Constantin
pdz, ETH Zürich
2019

-------------------------------------------------
Usage

With no previously trained network
python3 tracking.py
 --device=wanda
 --method=SLSD/FLSD
 --showMore=True
 --saveResult=False
 --startFrame=<int-OPTIONAL>
 --endFrame=<int-OPTIONAL>
 --ratio=<h/w of screen>
 --backlight=True(if screen is backlight)
 --weights=True(if weights exist)

"""
import cv2
import numpy as np
import pdb
import math
from time import time
import datetime
import csv
import pandas
import re
import os
import sys
import json
import skimage
import logging, coloredlogs
from scipy.spatial import distance as dist

from Helper.helper import *
from Helper.ui_helper import *
from Helper.lsd_helper import *
from Helper.check_helper import *
from Helper.visualize_helper import *
from Helper.logger import init_logger

# Root directory of the project
ROOT_DIR = os.path.abspath('./')

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn.config import Config
from mrcnn import model as modellib, utils

# Path to trained weights file
COCO_WEIGHTS_PATH = os.path.join(ROOT_DIR, "weights/mask_rcnn_coco.h5")

# Directory to save logs and model checkpoints, if not provided
# through the command line argument --logs
DEFAULT_LOGS_DIR = os.path.join(ROOT_DIR, "logs")

# # global Variables
SUB_ROI = []
OLD_PTS = []
WIDTH = 0
HEIGHT = 0

JSON_DATA = []

def click(event, x, y, flags, param):
    # grab references to the global variables
    global SUB_ROI

    # if the left mouse button was clicked, record the starting
    # (x, y) coordinates and indicate that cropping is being
    # performed
    if event == cv2.EVENT_LBUTTONDOWN:
        SUB_ROI.append((x,y))
        cv2.circle(frame_copy, (x,y), 3, (255, 0, 0), -1)
        if len(SUB_ROI) == 1:
            print('Top-Left: ', SUB_ROI[0])
        if len(SUB_ROI) == 2:
            print('Top-Right: ', SUB_ROI[1])
        if len(SUB_ROI) == 3:
            print('Bottom-Right: ', SUB_ROI[2])
        if len(SUB_ROI) == 4:
            print('Bottom-Left: ', SUB_ROI[3])
            print('SUB_ROI:', SUB_ROI)

def write_to_file(i, pts, h, w, device, method):
    global JSON_DATA

    all_x = []
    all_y = []
    for p in pts:
        all_x.append(p[0])
        all_y.append(p[1])

    with open(os.path.join(ROOT_DIR,'text_files/tracked_pts_%s_%s.json' %(device, method)), 'w') as json_file:
        data = {
            'frame_nb': i,
            'shape_attributes':{
                "name": 'polygon',
                "all_points_x": np.array(all_x).tolist(),
                "all_points_y": np.array(all_y).tolist()
            },
            'height': h,
            'width': w
        }
        JSON_DATA.append(data)
        json.dump(JSON_DATA, json_file)

    with open(os.path.join(ROOT_DIR,'text_files/tracked_pts_%s_%s.csv' %(device, method)), mode='a', encoding='utf-8') as eval_file:
        eval_writer = csv.writer(eval_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        if i == 0:
            eval_writer.writerow(['frame', 'pts'])

        eval_writer.writerow(['%s' %i, '%s' %pts])

def get_Mask_RCNN_detection(frame, is_found, image_enhancement):
    splash = None
    mask_tuples = []
    # Detect mask
    r = model.detect([frame], verbose=1)[0]
    fc = frame.copy()
    if len(r['scores'] > 0):
        # Proceed if mask was found
        is_found = True
        cnn_mask = r['masks']

        # Display the mask as a color splash on the image
        cnn_mask = (np.sum(cnn_mask, -1, keepdims=True) >= 1)
        redImg = np.zeros(frame.shape, frame.dtype)
        redImg[:,:] = (0, 0, 255)
        gray = skimage.color.gray2rgb(skimage.color.rgb2gray(frame)) * 255
        splash = np.where(cnn_mask, redImg, gray).astype(np.uint8)

        # Get the coordinates of the mask
        mask_coords = np.asarray(r['masks']).nonzero()
        mask_tuples = []
        for x,y in zip(mask_coords[0], mask_coords[1]):
            mask_tuples.append([y,x])

    return is_found, mask_tuples, splash


class DeviceConfig(Config):
    """Configuration for training on the toy  dataset.
    Derives from the base Config class and overrides some values.
    """
    # Give the configuration a recognizable name
    NAME = "device"

    # We use a GPU with 12GB memory, which can fit two images.
    # Adjust down if you use a smaller GPU.
    IMAGES_PER_GPU = 2

    # Number of classes (including background)
    NUM_CLASSES = 1 + 1  # Background + balloon

    # Number of training steps per epoch
    STEPS_PER_EPOCH = 100

    # Skip detections with < 90% confidence
    DETECTION_MIN_CONFIDENCE = 0.9

if __name__ == '__main__':
    import argparse

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description='Screen tracking')
    parser.add_argument("--device",
                        metavar="<device>",
                        help="device name")
    parser.add_argument("--weights", required=False,
                        default=False,
                        metavar="<weights>",
                        help="Path to weight file")
    parser.add_argument("--saveResult", required=False,
                        default=False,
                        metavar="<saveResult>",
                        help="save results")
    parser.add_argument("--showMore", required=False,
                        default=False,
                        metavar="<showMore>",
                        help="show More Info")
    parser.add_argument("--startFrame", required=False,
                        default=0,
                        metavar="<startFrame>",
                        help='Set first frame of Video')
    parser.add_argument("--endFrame", required=False,
                        default=None,
                        metavar="<endFrame>",
                        help='Set last frame of Video')
    parser.add_argument("--backlight", required=False,
                        default=False,
                        metavar="<backlight>",
                        help='screen technology')
    parser.add_argument("--ratio", required=True,
                        default=False,
                        metavar="<ratio>",
                        help='H/W')
    parser.add_argument("--method", required=True,
                        default=False,
                        metavar="<method>",
                        help='SLSD/FLSD')

    # Set the logger
    now = datetime.datetime.now()
    y = now.year
    m = now.month
    d = now.day
    fileName = '%s_%s_%s' %(y,m,d)
    init_logger(fileName)
    logger = logging.getLogger()

    # Get the command line arguments
    args = parser.parse_args()

    show = True if args.showMore == 'True' else False
    saveResult = True if args.saveResult == 'True' else False
    image_enhancement = True if args.backlight == 'True' else False
    video_file_path = '%s' %os.path.join(ROOT_DIR, 'assets/Videos/video_%s.avi' %args.device)
    gaze_file_path = '%s' %os.path.join(ROOT_DIR, 'text_files/gaze_pts_%s.csv' %args.device)
    weights_exist = True if args.weights == 'True' else False

    print('Video file path: ', video_file_path)
    print('Gaze file path: ', gaze_file_path)
    if weights_exist:
        weights_file_path = '%s' %os.path.join(ROOT_DIR, 'weights/mask_rcnn_%s.h5' %args.device)
        print('Weights file path: ', weights_file_path)
    else:
        print('No weights exist')
    print('Save Results: ', saveResult)
    print('Device: ', args.device)
    print('Show More: ', show)

    # If weights exist configure the network
    if weights_exist:
        class InferenceConfig(DeviceConfig):
                # Set batch size to 1 since we'll be running inference on
                # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
                GPU_COUNT = 1
                IMAGES_PER_GPU = 1
        config = InferenceConfig()
        config.NAME = args.device
        config.display()

        model = modellib.MaskRCNN(mode="inference", config=config,
            model_dir=DEFAULT_LOGS_DIR)
        # Load weights
        print("Loading weights ", weights_file_path)
        model.load_weights(weights_file_path, by_name=True)

    # Get the synchronized gaze points
    gaze = get_gaze_pts(gaze_file_path)

    # Open Video and set to start frame
    cap = cv2.VideoCapture('./assets/Videos/video_%s.avi' %args.device) # %s' %file)
    if int(args.startFrame) != 0:
        cap.set(cv2.CAP_PROP_POS_FRAMES, int(args.startFrame))
    total_nb_frames = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    endFrame = int(args.endFrame) if args.endFrame is not None else total_nb_frames

    # Set parameters for lucas kanade optical flow
    lk_params = dict( winSize = (20,20),
                    maxLevel = 2,
                    criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))
    # Create some random colors for OF display
    color_OF = np.random.randint(0,255,(100,3))

    # Take first frame
    ret, old_frame = cap.read()
    # copy the first frame to counteract initial optical flow movement
    frame = old_frame.copy()

    old_gray = cv2.cvtColor(old_frame, cv2.COLOR_BGR2GRAY)
    mask = np.zeros_like(old_frame)

    ## INITIALIZE VARIABLES
    # Frame count of video
    i = int(cap.get(cv2.CAP_PROP_POS_FRAMES))
    # Frame count in gaze file
    gaze_counter = i-gaze[0]['frame']-1

    # Width of search grid
    EPSILON = 15
    # Scale factor for EPSILON depending on distance to object (i.e. height and width)
    factor = 0.08
    RATIO = float(args.ratio)

    # OF tracking points
    frame_coords = []

    # time keeping variables
    times = []
    t_start = time()

    # CNN boolean if screen is found
    is_found = True
    is_initial = True
    is_cnn = False
    # Boolean check points
    is_ok = False
    is_ok_location = True
    is_ok_mask_shape = True
    is_ok_pts_shape = True
    is_huge_move = False
    is_well_segmented = True
    has_points = True
    has_lines = True
    has_lines_all_sides = True
    # Counter for multiple fails in a row
    TRY_COUNTER = 0

    # Initialize progess bar displayed in command line
    k = len(gaze)
    printProgressBar(gaze_counter, k, prefix = 'Progress:', suffix = 'Complete', length = 50)
    logger.debug('\n')
    logger.info('START')

    #  Check if video exists
    if (cap.isOpened()== False):
        logger.critical('Error opening video stream or file')

    # Play Video if gaze points exist
    while (cap.isOpened()) and gaze_counter < len(gaze)-1 and i <= endFrame:
        # Update progress bar
        printProgressBar(gaze_counter + 1, k, prefix = 'Progress:', suffix = 'Complete', length = 50)

        # Check if there are gaze points for specifix frame, else jump to next frame
        if len(gaze[gaze_counter]['pts']) > 0:
            # Set video to corresponding frame
            cap.set(cv2.CAP_PROP_POS_FRAMES, gaze[gaze_counter]['frame'])
            # Update the frame count variable
            i = int(cap.get(cv2.CAP_PROP_POS_FRAMES))

            #Read the frame
            ret, frame = cap.read()

            #Check if frame was read correctly (ret: bool)
            if not ret:
                break

            if ret:
                # Video interaction variable 1
                key = cv2.waitKey(1) & 0xff

                # Get cooresponding gaze points
                gaze_pts = gaze[gaze_counter]['pts']
                # Show gaze points
                show_gaze_pts(gaze_pts, frame)

                # Get shape of frame
                HEIGHT, WIDTH = frame.shape[:2]

                # Get gray scale frame
                frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

                # quit the loop and stop the video
                if key == ord('q'):
                    logger.info('Quit the programm')
                    break

                # Pause the Video
                pause_video(key, frame)

                # Reset the points
                if key == ord('r'):
                    RESET_COUNTER += 1
                    logger.info('Reset points')
                    of_counter = 0
                    frame_coords = []
                    SUB_ROI = []

                # Display the frame
                cv2.imshow('FRAME', frame)

                # Initialize the tracker at the start and whenever there is a problem
                if not is_ok or is_initial:
                    # Use Mask RCNN if errors and weights flag is set
                    if not is_initial and weights_exist:
                        logger.info('Start of Mask R-CNN recovery')
                        is_found, mask_tuples, splash = get_Mask_RCNN_detection(frame, is_found, image_enhancement)
                        if is_found:
                            logger.debug('Device was found')

                            # EPSILON=int(H*factor)
                            is_ok = True
                            is_cnn = True
                            is_ok_location = True
                            is_found = False
                        else:
                            logger.warning('No device detected')
                            logger.info('Jump to next fixation')

                            idx = next(i for i, x in enumerate(gaze) if gaze[i]['frame'] > gaze_counter and len(gaze[i-1]['pts']) == 0 and len(gaze[i-2]['pts']) == 0 and len(gaze[i-3]['pts']) == 0 and len(gaze[i]['pts']) > 0)
                            new_frame = gaze[idx]['frame']
                            cap.set(cv2.CAP_PROP_POS_FRAMES, new_frame)
                            frame_coords = []
                            SUB_ROI = []
                            gaze_counter = new_frame
                    else:
                        # User Interaction: User selects points
                        frame_copy = frame.copy()
                        while True:
                            key3 = cv2.waitKey(1) or 0xff
                            cv2.imshow('CLICK', frame_copy)
                            cv2.namedWindow('CLICK')
                            cv2.setMouseCallback('CLICK', click)
                            # Confirm with 'c'
                            if key3 == ord('c'):
                                break
                            # Reset with 'r'
                            if key3 == ord('r'):
                                SUB_ROI = []
                                frame_copy = frame.copy()
                                logger.info('Resetting the area')
                            # Jump to next fixation with 'n'
                            if key3 == ord('n'):
                                logger.debug('Jump to next fixation')

                                idx = next(i for i, x in enumerate(gaze) if gaze[i]['frame'] > gaze_counter and len(gaze[i-1]['pts']) == 0 and len(gaze[i-2]['pts']) == 0 and len(gaze[i-3]['pts']) == 0 and len(gaze[i]['pts']) > 0)
                                new_frame = gaze[idx]['frame']
                                cap.set(cv2.CAP_PROP_POS_FRAMES, new_frame)
                                frame_coords = []
                                SUB_ROI = []
                                gaze_counter = new_frame
                                break
                        # If 4 points are selected set the optical flow coords and EPSILON
                        if len(SUB_ROI) == 4:
                            frame_coords = np.array(SUB_ROI, dtype=np.float32)
                            H = dist.euclidean(SUB_ROI[1], SUB_ROI[2])
                            W = dist.euclidean(SUB_ROI[0], SUB_ROI[1])
                            EPSILON=int(H*factor)
                            EPSILON_W=int(W*factor)
                            EPSILON = 15 if EPSILON > 15 else EPSILON
                            EPSILON_W = 15 if EPSILON_W > 15 else EPSILON_W
                            cv2.destroyWindow('CLICK')
                            is_initial = False
                            is_ok = True

                    old_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)[:]

                # If no error occured or correctly initialized
                if is_ok:
                    if not is_cnn:
                        logger.debug('Calculate Optical Flow movement')

                        p1, st, err = cv2.calcOpticalFlowPyrLK(old_gray, frame_gray, frame_coords, None, **lk_params)
                        # Check if Optical flow error is above threshold (8)
                        is_of_error = check_error(err)
                        # Check the movement of all individual points
                        xmvmts, ymvmts, mvmts = monitor_mvmt(p1, SUB_ROI)
                        # Check for outliers in the 4 point movements
                        idx = check_for_outliers(xmvmts, ymvmts)
                        # Check if any point had a huge move (> 50 px)
                        is_huge_move = check_for_huge_mvmt(xmvmts,ymvmts)
                        # Copy points for processing
                        pts_for_process = p1[:]

                        if len(idx) == 1:
                            logger.debug('Adjusting one point')
                            # Get average movement of good points and adjust the outlier
                            avg_mov = get_avg_good_mvmts(xmvmts, ymvmts, idx)
                            pts_for_process[idx[0]] = pts_for_process[idx[0]] + avg_mov
                        elif len(idx) >= 2 and is_of_error:
                            # If optical flow error confirms more than 2 outliers then break and jump to next frame
                            has_points = False

                    elif is_cnn:
                        # If CNN detection then treat the mask coordinates as points for process
                        pts_for_process = mask_tuples[:]

                    # If above steps are passed continue with line segment detection
                    if (has_points and not is_huge_move) or (is_cnn and is_found):
                        logger.info('Start of LSD, Frame: %s' %i)
                        # Show the optical flow movement
                        if not is_cnn: show_OF_mvmt(mask, frame, pts_for_process, frame_coords, color_OF)
                        # Get the inner and outer mask
                        outer, inner, img_bbox, img_extension = get_search_area(pts_for_process,frame, is_cnn, EPSILON)
                        if is_cnn:
                            H = dist.euclidean(outer[1], outer[2])
                            W = dist.euclidean(outer[0], outer[1])
                        # Check if the shape of the mask is withint the range of the initial ratio (-+ 20%)
                        is_ok_mask_shape, r1, r2  = check_shape(outer, RATIO)
                        if is_ok_mask_shape:
                            logger.debug('Shape of mask within boundaries')
                            # Enhance image (brightness and contrast) if backlight flag set
                            image = get_enhanced_image(frame) if image_enhancement else frame[:]
                            # Detect edges with Canny edge detector
                            edged = get_Canny(image, image_enhancement)
                            # Apply outer mask
                            res_outer = get_mask(outer,edged,frame)
                            # Apply înner mask
                            res_inner = get_mask(inner,res_outer,frame,inverse=True)
                            # Detect line segments and return True if lines exist, lines, and line segment detector object
                            has_lines, lines, lsd = get_lines(res_inner)
                            if has_lines:
                                logger.debug('Line Segment Detection Successfull')
                                # Display the lines
                                drawn_img = draw_lines(lsd, lines,frame)
                                # Vectorize the lines
                                vectors = get_vectors(lines)
                                # Get the angles compared to horizontal axis
                                angles = get_angles(vectors)
                                # Segment the group of lines in 2 based on their angles (more or less 90° apart)
                                segmented_by_angle = get_segmented_by_angle_kmeans(angles, lines)
                                is_well_segmented = True if segmented_by_angle is not None else False
                                if is_well_segmented:
                                    logger.debug('Segmentation by angle successfull')
                                    # Filter the lines by lenght (normed to device size)
                                    long_lines = get_lines_filtered_by_length(segmented_by_angle, H)
                                    # Display the segmented lines
                                    seg_by_angle_img = show_seg_by_angle(long_lines,frame)
                                    # Get the center points for each lines
                                    center_pts = get_center_pts(long_lines)
                                    # Display the center of lines
                                    seg_centers_img = show_seg_centers(center_pts, seg_by_angle_img)
                                    # Segment each group again by their coordinates
                                    segmented_by_coords = get_segmented_by_coordinates_kmeans(center_pts, long_lines)
                                    is_well_segmented_coord = True if segmented_by_coords is not None else False
                                    if is_well_segmented_coord:
                                        # Sort the lines according to their position
                                        top_lines, right_lines, bottom_lines, left_lines, drawn_im = get_sorted_lines(segmented_by_coords,frame)
                                        lines_array = [top_lines, right_lines, bottom_lines, left_lines]
                                        # Check if all side have at least one line
                                        has_lines_all_sides = check_all_sides_have_lines(lines_array)
                                        if has_lines_all_sides:
                                            # get the main lines of each side
                                            main_lines, lengths, normed_lengths = get_main_lines(lines_array)
                                            # get the average line of each side
                                            w_avg_lines = get_weighted_avg_lines(lines_array, lengths, normed_lengths)
                                            # Display main and average line
                                            main_lines_img = show_main_lines(main_lines, w_avg_lines, frame)
                                            # Get the intersections of main lines
                                            intersections = get_intersections(main_lines)
                                            # Display intersections of main lines
                                            intersections_img = show_intersections(intersections, main_lines_img)
                                            # Sort the intersections clockwise (TL, TR, BR, BL)
                                            sorted_inter = sortpts_clockwise(np.array(intersections, dtype='int32'))
                                            # Check if shape is with range of initial ratio (-+ 20%)
                                            is_ok_pts_shape, r1, r2  = check_shape(sorted_inter, RATIO)
                                            if is_ok_pts_shape:
                                                # If CNN no prior location of points so not checking if intersections are within allowed range
                                                if not is_cnn:
                                                    # Check if intersections are within the mask region
                                                    indices, image = check_intersection_location(sorted_inter,pts_for_process,intersections_img, EPSILON)
                                                    is_ok_location = True if len(indices) == 0 else False

                                                if is_ok_location or is_cnn:
                                                    if is_cnn:
                                                        # Reset the Try counter
                                                        TRY_COUNTER = 0
                                                        logger.info('CNN Recovery Successfull')

                                                    if saveResult:
                                                        # Save the information to files
                                                        write_to_file(i, sorted_inter, HEIGHT, WIDTH, args.device, args.method)

                                                    logger.debug('Found Intersections in first attempt')
                                                    # Set  variables for next iteration
                                                    frame_coords = np.array(sorted_inter, dtype='float32')
                                                    OLD_PTS = SUB_ROI[:]
                                                    SUB_ROI = np.array(frame_coords,dtype='int32')
                                                    H = dist.euclidean(SUB_ROI[1], SUB_ROI[2])
                                                    W = dist.euclidean(SUB_ROI[0], SUB_ROI[1])
                                                    is_initial = False
                                                    is_ok = True
                                                    is_cnn = False
                                                    TRY_COUNTER = 0
                                                    EPSILON=int(H*factor)
                                                    EPSILON = 15 if EPSILON > 15 else EPSILON
                                                    EPSILON_W=int(W*factor)
                                                    EPSILON_W = 15 if EPSILON_W > 15 else EPSILON_W

                                                    old_gray = frame_gray.copy()
                                                else:
                                                    # If intersections not within mask area redo with average line
                                                    logger.warning('Intersections out of allowed zone')

                                                    intersections = get_intersections(w_avg_lines)
                                                    intersections_img = show_intersections(intersections, main_lines_img)

                                                    sorted_inter = sortpts_clockwise(np.array(intersections, dtype='int32'))
                                                    indices, image = check_intersection_location(sorted_inter,pts_for_process,intersections_img, EPSILON)
                                                    is_ok_location = True if len(indices) == 0 else False

                                                    if is_ok_location:
                                                        if saveResult:
                                                            write_to_file(i, sorted_inter, HEIGHT, WIDTH, args.device, args.method)

                                                        logger.debug('Found Intersections in second attempt')
                                                        frame_coords = np.array(sorted_inter, dtype='float32')
                                                        OLD_PTS = SUB_ROI[:]
                                                        SUB_ROI = np.array(frame_coords,dtype='int32')
                                                        H = dist.euclidean(SUB_ROI[1], SUB_ROI[2])
                                                        W = dist.euclidean(SUB_ROI[0], SUB_ROI[1])
                                                        is_initial = False
                                                        is_ok = True
                                                        is_cnn = False
                                                        TRY_COUNTER = 0
                                                        EPSILON=int(H*factor)
                                                        EPSILON = 15 if EPSILON > 15 else EPSILON
                                                        EPSILON_W=int(W*factor)
                                                        EPSILON_W = 15 if EPSILON_W > 15 else EPSILON_W

                                                        old_gray = frame_gray.copy()

            # Handle Errors
            if is_huge_move or not is_well_segmented or not is_well_segmented_coord or not has_points or not has_lines or not is_ok_location or not is_ok_mask_shape or not has_lines_all_sides or not is_ok_pts_shape:
                # Handle all the errors that occur and display information
                if not has_points:
                    logger.warning('Lost more than 2 points')
                    logger.warning('xmvmts: %s, ymvmts: %s, mvmts: %s' %(xmvmts, ymvmts, mvmts))
                    logger.warning('idx: %s' %idx)
                    logger.warning('err: %s' %err)
                elif not has_lines:
                    logger.warning('No Line segments detected')
                elif is_huge_move:
                    logger.warning('Huge movements')
                    logger.warning('xmvmts: %s, ymvmts: %s, mvmts: %s' %(xmvmts, ymvmts, mvmts))
                elif not is_well_segmented:
                    logger.warning('No definite segmentation')
                elif not is_well_segmented_coord:
                    logger.warning('No definite segmentation')
                elif not is_ok_location:
                    logger.warning('Intersections too far away')
                    logger.warning('epsilon H, W : %s, %s' %(EPSILON, EPSILON_W))
                elif not is_ok_mask_shape:
                    TRY_COUNTER += 1
                    logger.warning('Mask Shape not ok')
                    logger.warning('RATIO: %s, r1: %s, r2: %s' %(RATIO, r1, r2))
                    if TRY_COUNTER > 3:
                        idx = next(i for i, x in enumerate(gaze) if gaze[i]['frame'] > gaze_counter and len(gaze[i-1]['pts']) == 0 and len(gaze[i-2]['pts']) == 0 and len(gaze[i-3]['pts']) == 0 and len(gaze[i]['pts']) > 0)
                        gaze_counter = gaze[idx]['frame']
                        logger.warning('Failed 3 times in a row. Jumping to next fixation')
                elif not is_ok_pts_shape:
                    TRY_COUNTER += 1
                    logger.warning('Points Shape not ok')
                    logger.warning('RATIO: %s, r1: %s, r2: %s' %(RATIO, r1, r2))
                    if TRY_COUNTER > 3:
                        idx = next(i for i, x in enumerate(gaze) if gaze[i]['frame'] > gaze_counter and len(gaze[i-1]['pts']) == 0 and len(gaze[i-2]['pts']) == 0 and len(gaze[i-3]['pts']) == 0 and len(gaze[i]['pts']) > 0)
                        gaze_counter = gaze[idx]['frame']
                        logger.warning('Failed 3 times in a row. Jumping to next fixation')
                elif not has_lines_all_sides:
                    logger.warning('No lines all sides')
                    logger.warning('Lines Array: %s' %lines_array)


                # Reset the variables for next iteration
                frame_copy = frame.copy()
                SUB_ROI = []
                is_ok = False
                is_initial = False
                has_points = True
                is_well_segmented = True
                is_huge_move = False
                has_lines = True
                is_ok_location = True
                is_ok_mask_shape = True
                is_ok_pts_shape = True
            # Increase gaze counter
            gaze_counter += 1
            # Destroy all Windows
            cv2.destroyWindow('Seg by Angles')
            cv2.destroyWindow('Extension')
            cv2.destroyWindow('Lines')
            cv2.destroyWindow('Centers')
            cv2.destroyWindow('Intersections')
            cv2.destroyWindow('Mask Outer')
            cv2.destroyWindow('CNN Detection')
            cv2.destroyWindow('Main lines')
            cv2.destroyWindow('Edged')
            cv2.destroyWindow('Lines segmented')
            cv2.destroyWindow('Edges')
            cv2.destroyWindow('Intersections Main Lines')
            cv2.destroyWindow('Check Intersections')
            cv2.destroyWindow('IMAGE')
            cv2.destroyWindow('Mask R-CNN Detection')

        else:
            gaze_counter += 1

    logger.info('Completed')
    # When everything done, release the video capture object
    cap.release()
    t_end = time()
    cv2.destroyAllWindows()
    cv2.waitKey(1)
    total_time = t_end-t_start
