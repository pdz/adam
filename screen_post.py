"""
Screen postprocessing

Written by Constantin Ehrlich-Adám
pdz, ETH Zürich
2019
------------------------------------------------------------

Usage: run from the command line as such:
    # Apply Screen postprocessing and visualizaiton for given device
    python3 screen_post.py --device=device-name --method=method-name
"""


import cv2
import numpy as np
from matplotlib import pyplot as plt
import pdb
from math import *
import pandas
import os
import json
from Helper.helper import *
from time import time
import re
import csv

ROOT_DIR = './'

def show_sequence_chart(screens,names,device, method):
    x = np.linspace(0, len(screens), 1)
    fig = plt.figure()
    plt.figure(figsize=(10,8))

    plt.yticks(np.arange(len(names)),names)

    for l,s in enumerate(screens):
        plt.scatter(l,s)

    plt.savefig(os.path.join(ROOT_DIR,'Evaluation/postprocessing/%s_%s/screen_sequence_chart.png' %(device, method)))

def read_data(device, method):
    # Read file with mapped gaze points
    df = pandas.read_csv(os.path.join(ROOT_DIR,'text_files/filtered_screen_matching_%s_%s.csv' %(device, method)),sep=',')
    screens = []
    frames = []
    for s,f in zip(df['filtered_screen_index'], df['frame']):
        screens.append(s)
        frames.append(f)
    return screens, frames

def get_screen_names(device):
    filenames = sorted([f for f in os.listdir(os.path.join(ROOT_DIR,'assets/%s/screens' %device)) if (f.endswith('.png') or f.endswith('.jpg'))])
    names = []
    for f in filenames:
        tmp = f.split('.')
        names.append(tmp[0])

    return names
if __name__ == '__main__':
    import argparse

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description='Screen post.')
    parser.add_argument("--device",
                        metavar="<device>",
                        help="device name")
    parser.add_argument("--method",
                        metavar="<method>",
                        help="SLSD/FLSD")
    args = parser.parse_args()

    screen_names = get_screen_names(args.device)
    screens, frames = read_data(args.device, args.method)
    for i,(s,f) in enumerate(zip(screens, frames)):
        with open(os.path.join(ROOT_DIR,'text_files/screens_postprocessing_%s_%s.csv' %(args.device, args.method)), mode='a') as eval_file:
                eval_writer = csv.writer(eval_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                if i == 0:
                    eval_writer.writerow(['frame', 'screen_index', 'screen_name'])

                eval_writer.writerow(['%s' %f, '%s' %s, '%s' %screen_names[s]])

    show_sequence_chart(screens, screen_names, args.device, args.method)


