"""
Create AOIs

Written by Constantin Ehrlich-Adám
pdz, ETH Zürich
2019
------------------------------------------------------------

Usage: run from the command line as such:
    # Create aois.json file for specific device
     python3 create_aoi.py --device=name of the device
"""

import cv2
import os
import json
from Helper.helper import get_layout

ROOT_DIR = './'
JSON_DATA = []

if __name__ == '__main__':
    import argparse

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description='Create Areas of Interest (AOI)')
    parser.add_argument("--device",required=True,
                        metavar="<device>",
                        help="device name")
    args = parser.parse_args()

    # Get Layout
    layout = get_layout(args.device, ROOT_DIR)
    copy = layout.copy()
    # cv2.imshow('Layout for %s' %args.device, layout)
    # cv2.waitKey(0)
    # Select ROI
    more = True
    boxes = []
    names = []
    while more:
        # Drag a box as AOI
        box = cv2.selectROI("AOI", copy, fromCenter=False,
                showCrosshair=True)
        boxes.append(box)
        print('Set name for AOI')
        # Give the AOI a name
        name = input()
        names.append(name)
        print(box, name)
        # Display name and AOI
        cv2.rectangle(copy, box, (0,0,255), 4)
        cv2.putText(copy, '%s' %name,
                        (box[0],box[1]-10),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        0.5,
                        (0,0,255))

        print('More AOIs ? (y/n):')
        # Redo if user wants more AOIs
        yesno = input()
        more = True if yesno == 'y' else False
    print(boxes, names)

    # Save all AOIs with their names in JSON file
    for box,name in zip(boxes,names):
        cv2.rectangle(copy, box, (0,0,255), 4)
        cv2.putText(copy, '%s' %name,
                        (box[0],box[1]-10),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        0.5,
                        (0,0,255))
        data = {
            'name': name,
            'top-left': [box[0], box[1]],
            'w,h': [box[2], box[3]],
        }
        JSON_DATA.append(data)

    with open(os.path.join(ROOT_DIR,'assets/%s/aois.json' %args.device), 'w') as json_file:
        json.dump(JSON_DATA, json_file)
