"""
Screen matching

Ehrlich-Adám Constantin
pdz, ETH Zürich
2019

-------------------------------------------------
Usage

With no previously trained network
python3 screen_matching.py --device=device-name --method=method-name --saveResults=bool --showMore=bool

"""

import cv2
import os
import numpy as np
import imutils
import pdb
from math import *
import glob
import csv
import scipy.signal as ss
from Helper.helper import printProgressBar
from time import time
import re

MIN_MATCH_COUNT = 10
ROOT_DIR = './'

def get_screen_names(device):
    return sorted([f for f in os.listdir(os.path.join(ROOT_DIR,'assets/%s/screens' %device)) if (f.endswith('.png') or f.endswith('.jpg'))])

def get_screens(screen_names,device):
    screens = []
    for name in screen_names:
        screens.append(cv2.imread(os.path.join(ROOT_DIR, 'assets/%s/screens/%s' %(device, name))))
    return screens

def brisk_screen_matching(screens, dst):
    good_matches = []
    sift_matches = []
    for screen in screens:
        # For every screen in database do sift matching with image
        resized_screen = imutils.resize(screen,width=dst.shape[1], height=dst.shape[0])
        sift = cv2.xfeatures2d.SIFT_create(nfeatures=0,
                                           nOctaveLayers=6,
                                           contrastThreshold = 0.06,
                                           edgeThreshold = 10,
                                           sigma = 0.6)

        # find the keypoints and descriptors with SIFT
        kp0, des0 = sift.detectAndCompute(resized_screen,None)
        kp1, des1 = sift.detectAndCompute(dst,None)

        # Method to match the keypoints
        FLANN_INDEX_KDTREE = 0
        index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
        search_params = dict(checks = 50)
        flann = cv2.FlannBasedMatcher(index_params, search_params)

        matches = flann.knnMatch(des0,des1,k=2)

        good = []
        for m,n in matches:
            # Minimum distance keypoints must have
            if m.distance < 0.6*n.distance:
                good.append(m)
        good_matches.append(len(good))
        if len(good)>MIN_MATCH_COUNT:
            src_pts = np.float32([ kp0[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
            dst_pts = np.float32([ kp1[m.trainIdx].pt for m in good ]).reshape(-1,1,2)

            M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,10.0)
            matchesMask = mask.ravel().tolist()

            h,w = dst.shape[:2]
            pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)

        else:
            matchesMask = None
            M = np.ones((3,3))

        draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                       singlePointColor = None,
                       matchesMask = matchesMask, # draw only inliers
                       flags = 2)

        img3 = cv2.drawMatches(resized_screen,kp0,dst,kp1,good,None,**draw_params)
        sift_matches.append(img3)
    # Get screen index with highest match number
    max_good_matches_idx = np.argmax(good_matches)
    return max_good_matches_idx, sift_matches[max_good_matches_idx]


def get_warped_screens(filenames, device, method):
    path = os.path.join(ROOT_DIR,'Evaluation/warped/%s_%s' %(device, method))
    warped = []
    frames = []
    numbers = re.compile(r'(\d+)')
    for name in filenames:
        frames.append(int(numbers.split('%s' %name)[1]))
        warped.append(cv2.imread(os.path.join(ROOT_DIR,name),0))

    return warped, frames

def sort_filenames(device,method):
    import re
    numbers = re.compile(r'(\d+)')
    def numericalSort(value):
        parts = numbers.split(value)
        parts[1::2] = map(int, parts[1::2])
        return parts

    filenames = [img for img in glob.glob("Evaluation/warped/%s_%s/*.png" %(device, method))]
    filenames = sorted(glob.glob('Evaluation/warped/%s_%s/*.png' %(device, method)), key=numericalSort)
    return filenames

def filter_SIFT_index(index):
    # Median filter one and two
    filt1 = ss.medfilt(index,kernel_size=3)
    filt2 = ss.medfilt(filt1,kernel_size=7)
    return filt2

if __name__ == '__main__':
    import argparse

    print('Screen matching')

    parser = argparse.ArgumentParser(
        description='Screen matching')
    parser.add_argument('--device', required=True,
                        metavar="device",
                        help='Device Name')
    parser.add_argument('--method', required=True,
                        metavar="method",
                        help='SLSD/FLSD')
    parser.add_argument("--saveResults",
                        metavar="<saveResults>",
                        help="save results in a file")
    parser.add_argument("--showMore",
                        metavar="<showMore>",
                        help="Visualize what is happening")
    args = parser.parse_args()

    screen_names = get_screen_names(args.device)
    screens = get_screens(screen_names, args.device)
    filenames = sort_filenames(args.device, args.method)
    warped, frames = get_warped_screens(filenames, args.device, args.method)

    warped = warped[0:10]
    frames = frames[0:10]
    # Initialize progress bar for command line
    k = len(warped)
    printProgressBar(0, k, prefix = 'Progress:', suffix = 'Complete', length = 50)

    index = []
    for l,(img, fnb) in enumerate(zip(warped, frames)):
        img = cv2.resize(img,(int(img.shape[1]/2),int(img.shape[0]/2)))
        max_idx, img_sift = brisk_screen_matching(screens,img)
        index.append(max_idx)
        if args.showMore == 'True':
            cv2.imshow('SIFT', img_sift)
            cv2.waitKey(3)

        if args.saveResults == 'True':
            print('SAVING IMAGES AND INDICES')
            # cv2.imwrite(os.path.join(ROOT_DIR,'Evaluation/SIFT/SIFT_%s.png' %l), img_sift)

            with open(os.path.join(ROOT_DIR,'text_files/screen_matching_%s_%s.csv' %(args.device, args.method)), mode='a') as eval_file:
                eval_writer = csv.writer(eval_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                if l == 0:
                    eval_writer.writerow(['frame', 'screen_index'])

                eval_writer.writerow(['%s' %fnb, '%s' %max_idx])
        # Update Progress Bar
        printProgressBar(l + 1, k, prefix = 'Progress:', suffix = 'Complete', length = 50)

    print('FILTERING RESULTS')
    filtered = filter_SIFT_index(index)
    if args.saveResults == 'True':
        print('SAVING FILTERED RESULTS')
        for l, (idx, fnb) in enumerate(zip(filtered,frames)):
            with open(os.path.join(ROOT_DIR,'text_files/filtered_screen_matching_%s_%s.csv' %(args.device, args.method)), mode='a') as eval_file:
                eval_writer = csv.writer(eval_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                if l == 0:
                    eval_writer.writerow(['frame', 'filtered_screen_index'])

                eval_writer.writerow(['%s' %fnb, '%s' %int(idx)])

    print('DONE')

