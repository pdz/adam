## aDAM - How to use Mask R-CNN for full automation

## Create a dataset
In order to train a model, a dataset is required in the form of a training and a validation set.

Please make sure that you have the following folder structure.

<pre>
|-- device
    |--dataset
        |--train
        |--val
        -- device.py
</pre>

To create a dataset please execute:

```
python3 device.py create --device=nameOfTheDevice
--dataset=pathToDataset
--weights=coco
--video=video_nameOfTheDevice.avi
--size=sizeOfTrainingSet --file=tracked_pts_nameOfTheDevice_nameOfTheMethod.json
```

example of execution:
```
python3 device.py create --device=wanda --dataset=dataset --weights=coco --video=video_wanda.avi --size=200 --file=tracked_pts_wanda_SLSD.json
```

This looks up the tracked points, extracts the according frame from the video file. It then writes a json file called
* train/tracked_pts.json
* val/tracked_pts.json

Make sure they both exist.

## Train the network

Now that the dataset is created, you can proceed and train the network. Make sure to download the latest [coco weights](https://github.com/matterport/Mask_RCNN/releases) and copy them into the weights folder.
<pre>
|-- device
|-- weights
    -- mask_rcnn_coco.h5
</pre>

Execute the following:

```
python3 device.py create --device=nameOfTheDevice
--dataset=pathToDataset
--weights=coco
--video=video_nameOfTheDevice.avi
```

This will start the training of a model based on the coco weights. A folder will be created with the device name and a timestamp in the logs folder. Every epoch a intermediated weight file is generated.

Once the training is done get the latest weights file and copy it into the weights folder.
<pre>
|-- device
|-- weights
    -- mask_rcnn_coco.h5
    -- mask_rcnn_nameOfTheDevice.h5
</pre>

You can now proceed to evaluate a new video and set the weights flag = True in the tracking.py.

> N.B. Our models were trained using 15 epochs and 100 steps each. You can always adapt to your needs. in line 76 and line 223.

> Also, a potential optimization is image augmentation, to maximize variation. This procedure was not tested in this work. However an possible implementation is shown in line 210-220.