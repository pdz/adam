import cv2
import numpy as np
import csv
import json

def putInfoText(frame,version=1):
    if version == 1:
        cv2.putText(frame, 'Please select the 4 corner points of the screen CLOCKWISE (TL,TR,BR,BL)',
                    (50,50),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    1,
                    (255,255,255))
        cv2.putText(frame, 'When done press c',
                    (50,80),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    1,
                    (255,255,255))
    elif version == 2:
        cv2.putText(frame, 'Pause/Reset ROI: Press p',
                (50,50),
                cv2.FONT_HERSHEY_SIMPLEX,
                1,
                (255,255,255))
        cv2.putText(frame, 'Quit: Press q',
                (50,80),
                cv2.FONT_HERSHEY_SIMPLEX,
                1,
                (255,255,255))

def pause_video(key,frame):
    global SUB_ROI

    if key == ord('p'):
        while True:
            # Video Pause Interaction Variable
            key2 = cv2.waitKey(1) or 0xff

            cv2.putText(frame,'PAUSE',
                    (int(HEIGHT/2),int(WIDTH/2)),
                    cv2.FONT_HERSHEY_COMPLEX,
                    5,
                    (0,0,255))
            cv2.imshow('FRAME', frame)


            if key2 == ord('c'):
                break
                print('CONTINUE')

